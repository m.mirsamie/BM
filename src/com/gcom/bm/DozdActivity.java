package com.gcom.bm;

import android.os.Build;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class DozdActivity extends Activity {
	WebView mywebview;

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public void fixPro()
	{
		mywebview.getSettings().setAllowUniversalAccessFromFileURLs(true);
		mywebview.getSettings().setAllowFileAccessFromFileURLs(true);
	}
	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dozd);
		mywebview = (WebView) findViewById(R.id.w1);
		WebSettings webSettings = mywebview.getSettings();
		mywebview.addJavascriptInterface(new DozdClass(this), "DozdClass");
		webSettings.setJavaScriptEnabled(true);
		mywebview.loadUrl("file:///android_asset/dozd/dozd.html");
		if(android.os.Build.VERSION.SDK_INT==Build.VERSION_CODES.JELLY_BEAN)
			fixPro();

	}
}
