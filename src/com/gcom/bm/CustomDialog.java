package com.gcom.bm;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.ToggleButton;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class CustomDialog {
	int value;
	int id;
	Dialog dg;
	Context mContext;
	CustomDialog(int inId , int inValue, Context inContext){
		value = inValue;
		id = inId;
		mContext = inContext;
		generateDialog(true);
	}
	CustomDialog(int inId , int inValue, Context inContext,Boolean seekbarDialog){
		value = inValue;
		id = inId;
		mContext = inContext;
		generateDialog(seekbarDialog);
	}
	public void okClicked(int newVal)
	{
		//To be Overridden
	}
	public void cancelClicked(int oldVal)
	{
		//To be Overridden		
	}
	private void generateDialog(Boolean seekbarDialog)
	{
		dg = new Dialog(mContext);
		dg.setTitle("تغییر مقدار");
		LinearLayout llP = new LinearLayout(mContext);
		llP.setOrientation(LinearLayout.VERTICAL);
		LinearLayout llC = new LinearLayout(mContext);
		llC.setOrientation(LinearLayout.HORIZONTAL);
		if(seekbarDialog)
		{
			final TextView tv = new TextView(mContext);
			tv.setText(String.valueOf(value));
			final SeekBar sb = new SeekBar(mContext);
			sb.setProgress(value);
			sb.setMax(10);
			sb.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				@Override
				public void onStopTrackingTouch(SeekBar arg0) {
					tv.setText(String.valueOf(arg0.getProgress()));
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar arg0) {
					tv.setText("?");
				}
				
				@Override
				public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
					tv.setText(String.valueOf(arg0.getProgress()));
				}
			});
			Button okBtn = new Button(mContext);
			okBtn.setText("تأیید");
			okBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					okClicked(sb.getProgress());
					dg.dismiss();
				}
			});
			Button cancelBtn = new Button(mContext);
			cancelBtn.setText("رد");
			cancelBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					cancelClicked(value);
					dg.dismiss();
				}
			});
			llC.addView(cancelBtn);
			llC.addView(okBtn);
			llP.addView(tv);
			llP.addView(sb);
		}
		else
		{
			final ToggleButton tb = new ToggleButton(mContext);
			tb.setChecked(value != 0);
			Button okBtn = new Button(mContext);
			okBtn.setText("تأیید");
			okBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					okClicked(tb.isChecked()?1:0);
					dg.dismiss();
				}
			});
			Button cancelBtn = new Button(mContext);
			cancelBtn.setText("رد");
			cancelBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					cancelClicked(value);
					dg.dismiss();
				}
			});
			llC.addView(cancelBtn);
			llC.addView(okBtn);
			llP.addView(tb);
		}
		llP.addView(llC);
		dg.setContentView(llP);
		dg.setCanceledOnTouchOutside(false);
		/*
		final TextView tv = (TextView) ((Activity)mContext).findViewById(R.id.dg_txt);
		tv.setText(String.valueOf(value));					
		final SeekBar sb = (SeekBar) ((Activity)mContext).findViewById(R.id.dg_seek);
		sb.setProgress(value);
		sb.setMax(10);
		sb.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				tv.setText(String.valueOf(arg0.getProgress()));
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				tv.setText("?");
			}
			
			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				tv.setText(String.valueOf(arg0.getProgress()));
			}
		});
		Button okBtn = (Button) ((Activity)mContext).findViewById(R.id.dg_ok);
		okBtn.setText("تأیید");
		okBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				okClicked(sb.getProgress());
				dg.dismiss();
			}
		});
		Button cancelBtn = (Button) ((Activity)mContext).findViewById(R.id.dg_cancel);
		cancelBtn.setText("رد");
		cancelBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				cancelClicked(value);
				dg.dismiss();
			}
		});
		dg.setContentView(R.layout.dg);
		*/
		dg.show();
	}
}
