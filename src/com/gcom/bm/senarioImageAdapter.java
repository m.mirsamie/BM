package com.gcom.bm;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class senarioImageAdapter extends BaseAdapter {
    private Context mContext;

    public senarioImageAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(100, 100));
            imageView.setPadding(0, 0, 0, 0);
            imageView.setId(position+1);
            imageView.setOnClickListener(new OnClickListener() {				
				@Override
				public void onClick(View arg0) {
					ImageView hassan = (ImageView) arg0;
					int snumber =hassan.getId();
					if(MainActivity.allData.senario.id == snumber)
						MainActivity.allData.setSenario(0);
					else
						MainActivity.allData.setSenario(snumber);
				}
			});
        } else {
            imageView = (ImageView) convertView;
        }
        MainActivity.allData.refreshData();
        if(MainActivity.allData.senario.id == position+1 && MainActivity.allData.senario.id!=0 )
        	imageView.setImageResource(mThumbIds1[position]);
        else
        	imageView.setImageResource(mThumbIds[position]);
        return imageView;
    }

    public void refreshGrid()
    {
    	
    }
    
    private Integer[] mThumbIds = {
            R.drawable.s1, R.drawable.s2,
            R.drawable.s3, R.drawable.s4,
            R.drawable.s5, R.drawable.s6,
            R.drawable.s7, R.drawable.s8,
            R.drawable.s9
    };
    
    private Integer[] mThumbIds1 = {
            R.drawable.s1_selected, R.drawable.s2_selected,
            R.drawable.s3_selected, R.drawable.s4_selected,
            R.drawable.s5_selected, R.drawable.s6_selected,
            R.drawable.s7_selected, R.drawable.s8_selected,
            R.drawable.s9_selected
    };
    /*
    @SuppressWarnings({ "rawtypes" })
	private Class[] activities = {
	    	FireActivity.class,DozdActivity.class,
	    	GarmaActivity.class,LightActivity.class,
			CamsActivity.class,ScenarioActivity.class,
			SoundActivity.class,AboutActivity.class,
			SettingsActivity.class
	};
	*/
}