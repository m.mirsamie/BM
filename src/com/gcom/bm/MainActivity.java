package com.gcom.bm;

import java.util.Calendar;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.Toast;

public class MainActivity extends Activity {
	static DataCompiler allData;
	private IOClass ioclass;
	public class server_commands{
		final static byte read = 1;
		final static byte mis = 3;
		final static byte out = 4;
		final static byte dc = 5;
	}
	public static byte user_id = 1;
	private static final int SERVERPORT = 43002;
	//private static final String SERVER_IP = "94.183.242.78";
	private static final String SERVER_IP = "192.168.0.3";
	public static int bfs = 65;
	public static Boolean connected = false;
	public static int[] sts;
	public static int[] b={user_id,server_commands.read};
	public static int[] def_b = {user_id,server_commands.read};
	public static String sent="";
	public static int inputCount = 0;
	public static int connectPeriod = 100;
	public static String SockError = "";
	public static Boolean timerEn = true;
	public static Boolean crcok = false;
	public static Boolean preparingCommand = false;
	public static Boolean socketBussy = false;
	public static Boolean writeLogs = false; 
	public static Calendar exDate;
	public static int packetCounter = 0;
	public static Boolean isTcp = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		allData = new DataCompiler(MainActivity.this);
	    final GridView gridview = (GridView) findViewById(R.id.gridview);
	    gridview.setAdapter(new ImageAdapter(this));
	    ioclass = new IOClass(SERVER_IP,SERVERPORT,this);
	    ioclass.bfs = bfs;
	    ioclass.isTcp = isTcp;
	    final Handler handler = new Handler();
	    final Runnable r = new Runnable()
	    {
	        public void run() 
	        {
	        	try {
	        		if(writeLogs)
	        			sent += "packet : " + String.valueOf(packetCounter)+"\n";
	        		packetCounter++;
		        	readData();
		            handler.postDelayed(this, connectPeriod);					
				} catch (Exception e) {
					if(writeLogs)
						sent += "Runnable error\n";
				}
	        }
	    };
	    if(timerEn)
	    	handler.postDelayed(r, connectPeriod);
	}
	public void readData()
	{
		if(!connected)
			this.setTitle("HTE-Disconnected");
		else
			this.setTitle("HTE-Connected");
		if(!socketBussy)
			new Thread(new ClientThread()).start();
	}
	class ClientThread implements Runnable {
		@Override
		public void run() {
			socketBussy = true;
			try {				
				ioclass.open();
				int[] tmp_sts;
				if(writeLogs)
					sent += "Tread Log : TRUE";
				if(b[1]!=1)
					tmp_sts = ioclass.sendData(b);
				else
					tmp_sts = ioclass.sendData(def_b);
				
				MainActivity.crcok = ioclass.crcok; 
				if(ioclass.crcok && (b[1] != 0))
					sts = tmp_sts;
				connected = ioclass.connected;
				SockError = ioclass.SockError;
			} catch (Exception e) {
				if(writeLogs)
					sent += "TreadError : "+e.getMessage()+"\n";
			}
			socketBussy = false;
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId())
		{
			case R.id.version:
				PackageInfo pInfo;
				try {
					pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
					Toast.makeText(this, pInfo.versionName, Toast.LENGTH_LONG).show();
				} catch (NameNotFoundException e) {
					Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
				}
				return true;
			case R.id.logview:
				if(writeLogs)
				{
					writeLogs = false;
					Toast.makeText(this, "لاگ غیرفعال گردید", Toast.LENGTH_LONG).show();
				}
				else
				{
					writeLogs = true;
					Toast.makeText(this, "لاگ فعال گردید", Toast.LENGTH_LONG).show();					
				}
				return true;
			case R.id.about:
				System.exit(0);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

}