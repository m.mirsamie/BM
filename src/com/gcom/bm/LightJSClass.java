package com.gcom.bm;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class LightJSClass {
	Context mContext;
	public static int refreshRate = 100;

	public LightJSClass(Context inContext) {
		mContext = inContext;
	}
	public String lightsToJson(LightsClass[] inp)
	{
		String out = "";
		for(int i = 0;i < inp.length;i++)
		{
			LightsClass tmp = inp[i];
			out += ((out == "")?"":",") + "{ \"name\":\""+tmp.name+"\",\"lights\":[";
			String tmpOut = "";
			for(int j = 0;j < tmp.lights.length;j++)
				tmpOut += ((tmpOut!="")?",":"")+"{\"id\":"+String.valueOf(tmp.lights[j].id)+",\"value\":"+tmp.lights[j].value+",\"name\":\""+tmp.lights[j].name+"\"}";
			out += tmpOut+"]}";
		}
		return ("[" + out + "]");
	}
	
	public String relesToJson(RelesClass[] inp)
	{
		String out = "";
		for(int i = 0;i < inp.length;i++)
		{
			RelesClass tmp = inp[i];
			out += ((out == "")?"":",") + "{ \"name\":\""+tmp.name+"\",\"reles\":[";
			String tmpOut = "";
			for(int j = 0;j < tmp.reles.length;j++)
				tmpOut += ((tmpOut!="")?",":"")+"{\"id\":"+String.valueOf(tmp.reles[j].id)+",\"value\":"+tmp.reles[j].value+",\"name\":\""+tmp.reles[j].name+"\"}";
			out += tmpOut+"]}";
		}
		return ("[" + out + "]");
	}
	
	@JavascriptInterface
	public int getRefreshRate()
	{
		return(refreshRate);
	}
	
	@JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }
	
	@JavascriptInterface
	public String getSensors()
	{
		String out = "";
		MainActivity.allData.refreshData();
    	if(MainActivity.allData.dataPresented &&  !MainActivity.allData.offline)
    		out = "{\"allLights\" : "+lightsToJson(MainActivity.allData.allLights)+" , \"allReles\" : "+relesToJson(MainActivity.allData.allReles)+"}";
		return out;
	}

	@JavascriptInterface
	public void updateRele(String releId1,String value,String releGroupIndex)
	{
		MainActivity.allData.updateReles(Integer.valueOf(releId1),Integer.valueOf(value),Integer.valueOf(releGroupIndex));
		MainActivity.allData.refreshData();
	}
}
