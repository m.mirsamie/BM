package com.gcom.bm;

import com.ftdi.j2xx.D2xxManager;
import com.ftdi.j2xx.FT_Device;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

public class USBReadClass {
	Context mConext;
	D2xxManager ftdid2xx;
	int devCount = 0;
	public static D2xxManager ftD2xx = null;
    public boolean bReadThreadGoing = false;
	int DevCount = -1;
    byte[] ReadData = new byte[1024];
    byte[] WriteData = new byte[1024];
    int[] RealData = new int[1024];
    public static Boolean writeData = false; 
	FT_Device ftDevice = null;
	EventThread eventThread;
	long EventMask;
	int gg;
	int ff;
	Boolean kk;
	public static Boolean isReading = false;
	
    static Handler EventThread=new Handler() {
    	@Override
    	public void handleMessage(Message msg) {
    	}
	};
    
    class EventThread extends Thread {
    	Handler mHandler;
    	
    	EventThread(Handler h){
			this.setPriority(Thread.MAX_PRIORITY);
		}
    	
    	@Override
		public void run() {
			try{
	    		do{
		    		EventMask = ftDevice.getModemStatus();
		    		EventMask=EventMask & 16;
		    		if(EventMask == 16){
		    			kk=true;
		        		ff = ftDevice.getQueueStatus();
		        		if (ff>0){
		    	    		ftDevice.read(ReadData, ff);
		    	    		for(int i = 0;i < ReadData.length;i++)
		    	    			RealData[i] = charToInt(asciiToChar(ReadData[i]));
		    	    		Thread.sleep(10);
		    	    		if(writeData)
		    	    			ftDevice.write(WriteData);
		        		}
		    		}
	    		} while (kk==false);
    		}
    		catch (Throwable t) {
    		}
    		kk=false;
	   		eventThread = new EventThread(EventThread);
    		eventThread.start();
    	}
    }
	
    public char asciiToChar(byte inp)
    {
    	int tmp = posByte(inp);
    	char out = (char) tmp;
    	return out;
    }
    
    public int charToInt(char inp)
    {
    	int out = -1;
    	out = Integer.valueOf(inp);
    	return out;
    }
    public USBReadClass(Context inContext) {
		mConext = inContext;
    	try {
    		ftD2xx = D2xxManager.getInstance(mConext);
    	} catch (D2xxManager.D2xxException ex) {
    		ex.printStackTrace();
    	}
	}
	public void start()
	{
		if(DevCount <= 0)
			ConnectFunction();
	}
	public int posByte(byte inp)
	{
		return (inp <0)?inp+256:inp;
	}
	public String byteToString(byte[] inp)
	{
		String out = "";
		for(int i = 0;i < inp.length;i++)
			out += ((out=="")?"":",")+String.valueOf(posByte(inp[i]));
		return out;
	}
	
	public void read()
	{
		flushBuffer();
	}
	public void flushBuffer()
	{
		ReadData = new byte[1024] ;
		RealData = new int[1024];
	}
	public void write(byte[] inp)
	{
		writeData = false;
		int i;
		for(i = 0;i < inp.length;i++)
			WriteData[i] = inp[i];
		for(int j = i;j < WriteData.length;j++)
			WriteData[j] = ReadData[j];
		writeData = true;
	}
    public void ConnectFunction() {
		int openIndex = 0;

		if (DevCount > 0)
			return;

		DevCount = ftD2xx.createDeviceInfoList(mConext);

		if (DevCount > 0) {
			ftDevice = ftD2xx.openByIndex(mConext, openIndex);

			if(ftDevice == null)
			{
				Toast.makeText(mConext,"ftDev == null",Toast.LENGTH_LONG).show();
				return;
			}
			
			if (true == ftDevice.isOpen())
			{
				ftDevice.setBitMode((byte) 0, D2xxManager.FT_BITMODE_RESET);
				ftDevice.setBaudRate(57600);
				ftDevice.setDataCharacteristics(D2xxManager.FT_DATA_BITS_8,
				D2xxManager.FT_STOP_BITS_1, D2xxManager.FT_PARITY_NONE);
				ftDevice.setFlowControl(D2xxManager.FT_FLOW_NONE, (byte) 0x00, (byte) 0x00);
				ftDevice.setLatencyTimer((byte) 16);
				ftDevice.purge((byte) (D2xxManager.FT_PURGE_TX | D2xxManager.FT_PURGE_RX));
				if(false == bReadThreadGoing)
				{
					ftDevice.purge((byte) (D2xxManager.FT_PURGE_TX));
					ftDevice.restartInTask();
					ftDevice.setRts();
					ftDevice.setDtr(); 
					isReading = true;
			   		eventThread = new EventThread(EventThread);
		    		eventThread.start();
		    		bReadThreadGoing = true;
				}
			}
			else
			{
				Toast.makeText(mConext, "Need to get permission!",Toast.LENGTH_SHORT).show();
			}
		}
		else 
		{
			Toast.makeText(mConext, "j2xx : DevCount <= 0",Toast.LENGTH_SHORT).show();
		}
    }

}
