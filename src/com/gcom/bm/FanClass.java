package com.gcom.bm;

public class FanClass {
	public int id=-1;
	public String name="";
	public int dama = 0;
	public Boolean isAuto = false;
	public int autoValue = 0;
	public FanClass(int id, String name,int dama , Boolean isAuto,int autoValue)
	{
		this.id =id;
		this.name = name;
		this.dama = dama;
		this.isAuto = isAuto;
		this.autoValue = autoValue;
	}
}
