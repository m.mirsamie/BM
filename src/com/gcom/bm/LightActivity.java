package com.gcom.bm;

import android.os.Build;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;

public class LightActivity extends Activity {
	WebView mywebview;

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public void fixPro()
	{
		mywebview.getSettings().setAllowUniversalAccessFromFileURLs(true);
		mywebview.getSettings().setAllowFileAccessFromFileURLs(true);
	}
	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_light);
		mywebview = (WebView) findViewById(R.id.w3);
		WebSettings webSettings = mywebview.getSettings();
		mywebview.addJavascriptInterface(new LightJSClass(this), "LightJSClass");
		webSettings.setJavaScriptEnabled(true);
		mywebview.loadUrl("file:///android_asset/light/light.html");
		if(android.os.Build.VERSION.SDK_INT==Build.VERSION_CODES.JELLY_BEAN)
			fixPro();
	}
}
