package com.gcom.bm;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.view.Menu;
import android.widget.GridView;
import android.widget.ListAdapter;

public class ScenarioActivity extends Activity {
	public static ListAdapter la;
	public static GridView gridview;
	public static int connectPeriod = 1000;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scenario);
		la = new senarioImageAdapter(this);
	    gridview = (GridView) findViewById(R.id.Scenarioview);
	    gridview.setAdapter(la);
	    final Handler handler = new Handler();
	    final Runnable r = new Runnable()
	    {
	        public void run() 
	        {
	        	try {
		        	refreshGrid();
		            handler.postDelayed(this, connectPeriod);					
				} catch (Exception e) {
				}
	        }
	    };
    	handler.postDelayed(r, connectPeriod);
	}
	
	public static void refreshGrid()
	{
	    gridview.setAdapter(la);		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.scenario, menu);
		return true;
	}

}
