package com.gcom.bm;

public class ComplexClass {
	public int id=-1;
	public String name="";
	public Boolean stat=false;
	public Boolean auto = false;
	public int autoValue=0;
	public int value=0;
	public ComplexClass(int id ,String name,Boolean stat,Boolean auto,int autoValue,int value)
	{
		this.id = id;
		this.name = name;
		this.stat = stat;
		this.auto = auto;
		this.autoValue = autoValue;
		this.value = value;
	}
}
