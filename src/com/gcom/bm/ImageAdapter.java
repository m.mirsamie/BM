package com.gcom.bm;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;

    public ImageAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
    	/*
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View gridView;
			if (convertView == null) {
				gridView = new View(mContext);
				gridView = inflater.inflate(R.layout.mobile, null);
				TextView textView = (TextView) gridView
						.findViewById(R.id.grid_item_label);
				textView.setText(mobileValues[position]);
				textView.setTextColor(0xffffffff);
				ImageView imageView = (ImageView) gridView
						.findViewById(R.id.grid_item_image);
	            imageView.setContentDescription(String.valueOf(position));
				imageView.setImageResource(mThumbIds[position]);
	            imageView.setOnClickListener(new OnClickListener() {				
					@Override
					public void onClick(View arg0) {
						if(activities[Integer.valueOf(String.valueOf(arg0.getContentDescription()))]!=null)
						{
							Intent intent = new Intent();
							intent.setClass(mContext, activities[Integer.valueOf(String.valueOf(arg0.getContentDescription()))]);
							mContext.startActivity(intent);
						}
						else
							Toast.makeText(mContext, "دردست بروزرسانی", Toast.LENGTH_SHORT).show();
					}
				});
			} else {
				gridView = (View) convertView;
			}
	 
			return gridView;
		*/
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(100, 100));
            imageView.setPadding(0, 0, 0, 0);
            imageView.setContentDescription(String.valueOf(position));
            imageView.setOnClickListener(new OnClickListener() {				
				@Override
				public void onClick(View arg0) {
					Intent intent = new Intent();
					intent.setClass(mContext, activities[Integer.valueOf(String.valueOf(arg0.getContentDescription()))]);
					try {
						mContext.startActivity(intent);
					} catch (Exception e) {
						Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
					}
				}
			});
        } else {
            imageView = (ImageView) convertView;
        }
       	imageView.setImageResource(mThumbIds[position]);
        return imageView;
    }

    private Integer[] mThumbIds = {
            R.drawable.fire, R.drawable.dozd,
            R.drawable.garma, R.drawable.lights,
            R.drawable.cams,R.drawable.scenario, 
            R.drawable.sound,R.drawable.about, 
            R.drawable.settings
    };
    @SuppressWarnings({ "rawtypes" })
	private Class[] activities = {
	    	FireActivity.class,DozdActivity.class,
	    	GarmaActivity.class,LightActivity.class,
			CamsActivity.class,ScenarioActivity.class,
			SoundActivity.class,AboutActivity.class,
			SettingsActivity.class
	};
	/*
    private Integer[] mThumbIds = {
            R.drawable.fire, R.drawable.dozd,
            R.drawable.garma, R.drawable.lights,
            R.drawable.energy, R.drawable.cams,
            R.drawable.sms, R.drawable.internet,
            R.drawable.scenario, R.drawable.dvd,
            R.drawable.sound, R.drawable.weather,
            R.drawable.about, R.drawable.settings,
            R.drawable.gallery,R.drawable.game, 
            R.drawable.answer
    };
    @SuppressWarnings("rawtypes")
	private Class[] activities = {
    		FireActivity.class,DozdActivity.class,
    		GarmaActivity.class,LightActivity.class,
    		EnergyActivity.class,CamsActivity.class,
    		SmsActivity.class,InternetActivity.class,
    		ScenarioActivity.class,DvdActivity.class,
    		SoundActivity.class,WeatherActivity.class,
    		AboutActivity.class,SettingsActivity.class,
    		GalleryActivity.class,GameActivity.class,
    		AnswerActivity.class    		
    };
    private String[] mobileValues = {
    		"سیستم اعلام حریق","سیستم دزدگیر",
    		"سیستم حرارتی و برودتی","سیستم روشنایی",
    		"سیستم مدیریت انرژی","سیستم دوربین ها",
    		"سیستم پیام کوتاه","اتصال به اینترنت",
    		"سیستم دسته بندی","سیستم تصویری",
    		"سیستم صوتی","سیستم آب و هوا",
    		"درباره ما","تنظیمات سیستم",
    		"آلبوم عکس","سیستم بازی",
    		"سیستم منشی تلفنی"
    };
    */
}