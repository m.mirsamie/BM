package com.gcom.bm;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class FireActivity extends Activity {

	WebView mywebview;

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public void fixPro()
	{
		mywebview.getSettings().setAllowUniversalAccessFromFileURLs(true);
		mywebview.getSettings().setAllowFileAccessFromFileURLs(true);
	}

	@SuppressLint("SetJavaScriptEnabled")

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fire);
		mywebview = (WebView) findViewById(R.id.w2);
		WebSettings webSettings = mywebview.getSettings();
		mywebview.addJavascriptInterface(new FireClass(this), "FireClass");
		webSettings.setJavaScriptEnabled(true);
		mywebview.loadUrl("file:///android_asset/fire/fire.html");
		if(android.os.Build.VERSION.SDK_INT==Build.VERSION_CODES.JELLY_BEAN)
			fixPro();
	}
}