package com.gcom.bm;

public class DataBlockClass {
	public int StartIndex = 0;
	public int BlockLength = 1;
	public String dataClass = "LightClass";
	public String name = "";
	public int[] bits=null;
	public int scale = 2;
	public DataBlockClass(int inStart,int inBlock,String className,String inName)
	{
		StartIndex = inStart;
		BlockLength = inBlock;
		dataClass = className;
		name = inName;
	}
	public DataBlockClass(int inStart,int inBlock,String className,String inName,int[] inBits)
	{
		StartIndex = inStart;
		BlockLength = inBlock;
		dataClass = className;
		name = inName;
		bits = new int[inBits.length];
		bits = inBits;
	}
}
