package com.gcom.bm;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class DozdClass {
	Context mContext;
	public static int refreshRate = 1000;
	public static int blinkRate = 1500;
	
	public DozdClass(Context inContext) {
		mContext = inContext;
	}

	public String alarmSensorToJson(AlarmSensorClass[] inp)
	{
		String out = "";
		for(int i = 0;i < inp.length;i++)
		{
			AlarmSensorClass tmp = inp[i];
			out += ((out!="")?",":"")+"{\"id\":"+String.valueOf(tmp.id)+",\"isOn\":"+((tmp.isOn)?"true":"false")+"}";
		}
		return ("[" + out + "]");
	}
	
	@JavascriptInterface
	public int getRefreshRate()
	{
		return(refreshRate);
	}
	
	@JavascriptInterface
	public int getBlinkRate()
	{
		return(blinkRate);
	}
	
	@JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_LONG).show();
    }
	
	@JavascriptInterface
	public String getSensors()
	{
		String out = "";
		MainActivity.allData.refreshData();
    	if(MainActivity.allData.dataPresented &&  !MainActivity.allData.offline)
    		out = alarmSensorToJson(MainActivity.allData.alarmSensors);
		return out;
	}
	
	@JavascriptInterface
	public String getAlarmState()
	{
		String out = "NaN";
		MainActivity.allData.refreshData();
    	if(MainActivity.allData.dataPresented &&  !MainActivity.allData.offline)
    	{
    		Boolean tmp = MainActivity.allData.getAlarm();
    		out = (tmp)?"true":"false";
    	}
		return out;
	}
	
	@JavascriptInterface
	public void toggleAlarm()
	{
		MainActivity.allData.refreshData();
		String lastst = getAlarmState();
    	if(MainActivity.allData.dataPresented &&  !MainActivity.allData.offline)
    		MainActivity.allData.setAlarm((lastst=="true")?"false":"true");
	}
	
}
