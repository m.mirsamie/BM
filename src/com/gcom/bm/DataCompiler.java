package com.gcom.bm;

import java.util.concurrent.Callable;
import android.content.Context;

public class DataCompiler 
{
	public byte[] input;
	Context mContext;
	byte[] sendDataValue = {1,1};
	Boolean dataPresented = false;
	Boolean offline = true;	
	DataBlockClass[] dataBlockClass;
	int memOutIndex = 45;
	int memInIndex  = 15;
	int memMisIndex = 5;
	//--------------------Dimmers----------------------
	LightsClass[] allLights;
	LightClass[] lights;
	int lightStartIndex = 0;
	int lightCount = 2;
	String[] lightNames = {"D1","D2"};
	//-------------------------------------------------
	//---------------------Reles-----------------------
	RelesClass[] allReles;
	ReleClass[] reles;
	int releStartIndex = 8;
	int releCount = 10;
	String[] releNames = {"R1","R2","R3","R4","R5","R6","R7","R8","R9","R10"};
	//-------------------------------------------------
	//------------------FireSensors--------------------
	FireSensorClass[] fireSensors;
	int fireByte = 1;
	int fireOnBit = 2;
	int fireMuteBit = 3;
	public Boolean fireState = true;
	//-------------------------------------------------
	//------------------MotionSensors------------------
	MotionSensorClass[] motionSensors;
	//-------------------------------------------------
	//------------------AlarmSensors-------------------
	AlarmSensorClass[] alarmSensors;
	int alarmByte = 1;
	int alarmBit = 1;
	public Boolean alarmState = true;
	//-------------------------------------------------
	//------------------Senarios-----------------------
	SenarioClass senario;
	int senarioByteMis = 2;
	//-------------------------------------------------
	public DataCompiler(Context inContext)
	{
		mContext = inContext;
		lightCount = lightNames.length;
		releCount = releNames.length;
		dataBlockClass = new DataBlockClass[14];
		int[] releBits = {0,1};
		int[] alarmBits = {0};
		int[] fireBits = {1};
		int[] motionBits = {2};
		dataBlockClass[0] = new DataBlockClass(45, 2, "LightClass","DIM1");
		dataBlockClass[1] = new DataBlockClass(53, 5, "ReleClass","REL1",releBits);
		dataBlockClass[2] = new DataBlockClass(15, 1, "AlarmSensorClass","A1",alarmBits);
		dataBlockClass[3] = new DataBlockClass(21, 1, "AlarmSensorClass","A2",alarmBits);
		dataBlockClass[4] = new DataBlockClass(27, 1, "AlarmSensorClass","A3",alarmBits);
		dataBlockClass[5] = new DataBlockClass(33, 1, "AlarmSensorClass","A4",alarmBits);
		dataBlockClass[6] = new DataBlockClass(39, 1, "AlarmSensorClass","A5",alarmBits);
		dataBlockClass[7] = new DataBlockClass(15, 1, "FireSensorClass","F1",fireBits);
		dataBlockClass[8] = new DataBlockClass(21, 1, "FireSensorClass","F2",fireBits);
		dataBlockClass[9] = new DataBlockClass(27, 1, "FireSensorClass","F3",fireBits);
		dataBlockClass[10] = new DataBlockClass(33, 1, "FireSensorClass","F4",fireBits);
		dataBlockClass[11] = new DataBlockClass(39, 1, "FireSensorClass","F5",fireBits);
		dataBlockClass[12] = new DataBlockClass(15, 1, "MotionSensorClass","M1",motionBits);
		dataBlockClass[13] = new DataBlockClass(21, 1, "MOtionSensorClass","M2",motionBits);
		allLights = new LightsClass[1];
		allReles = new RelesClass[1];
		fireSensors = new FireSensorClass[5];
		motionSensors = new MotionSensorClass[2];
		alarmSensors = new AlarmSensorClass[5];
	}
	public int readBit(byte inp,int pos)
	{
		int out = 0;
		int bitMask = (int) Math.pow(2,pos);
		out = ((bitMask & inp) > 0)?1:0;
		return out;
	}
	public byte setBit(byte inp,int pos)
	{
		byte out = inp;
		byte bitMask = (byte) Math.pow(2,pos);
		out = (byte) (bitMask | inp);
		return out;
	}
	public byte resetBit(byte inp,int pos)
	{
		byte out = inp;
		byte bitMask = (byte) Math.pow(2,pos);
		out = (byte) (~bitMask & inp);
		return out;
	}
	public void refreshData()
	{
		try {			
			int j=0;
			int i;
			offline = !MainActivity.connected;
			if(MainActivity.sts.length > 0 && MainActivity.crcok)
			{
				int[] input = MainActivity.sts;
				dataPresented = true;
				int k=0;
				int lightI = 0;
				int lightId = 0;
				int releI = 0;
				int releId = 0;
				int fireSensorId = 0;
				int alarmSensorId = 0;
				int motionSensorId = 0;
				//------------read senario --------------
				byte senarioByte = 6;
				int senarioNumber=0;
				int[] Sts = MainActivity.sts;
				senarioNumber = Sts[senarioByte];
				senario = new SenarioClass(senarioNumber, "?????? ????? "+ String.valueOf(senarioNumber));
				for(k=0;k < dataBlockClass.length;k++)
				{
					if(dataBlockClass[k].dataClass.equals("LightClass"))
					{
						j = 0;
						int lightStartIndex1 = dataBlockClass[k].StartIndex;
						int lightCount1 = dataBlockClass[k].BlockLength;
						LightClass[] lights1 = new LightClass[lightCount1];
						for(i = lightStartIndex1;i < (lightCount1+lightStartIndex1);i++)
						{
							LightClass tmp = new LightClass(lightId,lightNames[j],input[i]);
							lights1[j] = tmp;
							j++;
							lightId++;
						}
						allLights[lightI] = new LightsClass(dataBlockClass[k].name, lights1);
						lightI++;
					}
					else if(dataBlockClass[k].dataClass.equals("ReleClass"))
					{
						int releStartIndex1 = dataBlockClass[k].StartIndex;
						int releCount1 = dataBlockClass[k].BlockLength;
						ReleClass[] reles1 = new ReleClass[dataBlockClass[k].BlockLength*dataBlockClass[k].bits.length];
						//-----------------------------Rele Block------------------------------------------
						for(i = releStartIndex1;i < ((2*releCount1)+releStartIndex1);i+=2)
							for(int w = 0;w < dataBlockClass[k].bits.length;w++)
							{
								byte releByte = (byte) input[i];
								byte val = (byte) readBit(releByte,dataBlockClass[k].bits[w]);
								ReleClass tmp = new ReleClass(releId, val,releNames[releId]);
								reles1[releId] = tmp;
								releId++;
							}
						//---------------------------------------------------------------------------------
						allReles[releI] = new RelesClass(dataBlockClass[k].name, reles1);
						releI++;
					}
					else if(dataBlockClass[k].dataClass.equals("AlarmSensorClass"))
					{
						int strIndex = dataBlockClass[k].StartIndex;
						int cont = dataBlockClass[k].BlockLength;
						for(i = strIndex;i < cont+strIndex;i++)
						{
							for(int w = 0;w < dataBlockClass[k].bits.length;w++)
							{
								byte dbyte = (byte) input[i];
								byte val = (byte) readBit(dbyte,dataBlockClass[k].bits[w]);
								AlarmSensorClass tmp = new AlarmSensorClass(alarmSensorId, (val == 1));
								alarmSensors[alarmSensorId] = tmp;
							}
						}
						alarmSensorId++;
					}
					else if(dataBlockClass[k].dataClass.equals("FireSensorClass"))
					{
						int strIndex = dataBlockClass[k].StartIndex;
						int cont = dataBlockClass[k].BlockLength;
						for(i = strIndex;i < cont+strIndex;i++)
						{
							for(int w = 0;w < dataBlockClass[k].bits.length;w++)
							{
								byte dbyte = (byte) input[i];
								byte val = (byte) readBit(dbyte,dataBlockClass[k].bits[w]);
								FireSensorClass tmp = new FireSensorClass(fireSensorId, (val == 1));
								fireSensors[fireSensorId] = tmp;
							}
						}
						fireSensorId++;
					}
					else if(dataBlockClass[k].dataClass.equals("MotionSensorClass"))
					{
						int strIndex = dataBlockClass[k].StartIndex;
						int cont = dataBlockClass[k].BlockLength;
						for(i = strIndex;i < cont+strIndex;i++)
						{
							for(int w = 0;w < dataBlockClass[k].bits.length;w++)
							{
								byte dbyte = (byte) input[i];
								byte val = (byte) readBit(dbyte,dataBlockClass[k].bits[w]);
								MotionSensorClass tmp = new MotionSensorClass(motionSensorId, (val == 1));
								motionSensors[motionSensorId] = tmp;
							}
						}
						motionSensorId++;
					}
				}
			}
			else
				dataPresented = false;
		} catch (Exception e) {
		}
	}

	public void refreshData(Callable<Void> fn)
	{
		try {			
			int j=0;
			int i;
			offline = !MainActivity.connected;
			if(MainActivity.sts.length > 0 && MainActivity.crcok)
			{
				int[] input = MainActivity.sts;
				dataPresented = true;
				getAlarm();
				int k=0;
				int lightI = 0;
				int lightId = 0;
				int releI = 0;
				int releId = 0;
				int fireSensorId = 0;
				int alarmSensorId = 0;
				int motionSensorId = 0;
				for(k=0;k < dataBlockClass.length;k++)
				{
					if(dataBlockClass[k].dataClass.equals("LightClass"))
					{
						j = 0;
						int lightStartIndex1 = dataBlockClass[k].StartIndex;
						int lightCount1 = dataBlockClass[k].BlockLength;
						LightClass[] lights1 = new LightClass[lightCount1];
						for(i = lightStartIndex1;i < (lightCount1+lightStartIndex1);i++)
						{
							LightClass tmp = new LightClass(lightId,lightNames[j],input[i]);
							lights1[j] = tmp;
							j++;
							lightId++;
						}
						allLights[lightI] = new LightsClass(dataBlockClass[k].name, lights1);
						lightI++;
					}
					else if(dataBlockClass[k].dataClass.equals("ReleClass"))
					{
						int releStartIndex1 = dataBlockClass[k].StartIndex;
						int releCount1 = dataBlockClass[k].BlockLength;
						ReleClass[] reles1 = new ReleClass[dataBlockClass[k].BlockLength*dataBlockClass[k].bits.length];
						//-----------------------------Rele Block------------------------------------------
						for(i = releStartIndex1;i < ((2*releCount1)+releStartIndex1);i+=2)
							for(int w = 0;w < dataBlockClass[k].bits.length;w++)
							{
								byte releByte = (byte) input[i];
								byte val = (byte) readBit(releByte,dataBlockClass[k].bits[w]);
								ReleClass tmp = new ReleClass(releId, val,releNames[releId]);
								reles1[releId] = tmp;
								releId++;
							}
						//---------------------------------------------------------------------------------
						allReles[releI] = new RelesClass(dataBlockClass[k].name, reles1);
						releI++;
					}
					else if(dataBlockClass[k].dataClass.equals("AlarmSensorClass"))
					{
						int strIndex = dataBlockClass[k].StartIndex;
						int cont = dataBlockClass[k].BlockLength;
						for(i = strIndex;i < cont+strIndex;i++)
						{
							for(int w = 0;w < dataBlockClass[k].bits.length;w++)
							{
								byte dbyte = (byte) input[i];
								byte val = (byte) readBit(dbyte,dataBlockClass[k].bits[w]);
								AlarmSensorClass tmp = new AlarmSensorClass(alarmSensorId, (val == 1));
								alarmSensors[alarmSensorId] = tmp;
							}
						}
						alarmSensorId++;
					}
					else if(dataBlockClass[k].dataClass.equals("FireSensorClass"))
					{
						int strIndex = dataBlockClass[k].StartIndex;
						int cont = dataBlockClass[k].BlockLength;
						for(i = strIndex;i < cont+strIndex;i++)
						{
							for(int w = 0;w < dataBlockClass[k].bits.length;w++)
							{
								byte dbyte = (byte) input[i];
								byte val = (byte) readBit(dbyte,dataBlockClass[k].bits[w]);
								FireSensorClass tmp = new FireSensorClass(fireSensorId, (val == 1));
								fireSensors[fireSensorId] = tmp;
							}
						}
						fireSensorId++;
					}
					else if(dataBlockClass[k].dataClass.equals("MotionSensorClass"))
					{
						int strIndex = dataBlockClass[k].StartIndex;
						int cont = dataBlockClass[k].BlockLength;
						for(i = strIndex;i < cont+strIndex;i++)
						{
							for(int w = 0;w < dataBlockClass[k].bits.length;w++)
							{
								byte dbyte = (byte) input[i];
								byte val = (byte) readBit(dbyte,dataBlockClass[k].bits[w]);
								MotionSensorClass tmp = new MotionSensorClass(motionSensorId, (val == 1));
								motionSensors[motionSensorId] = tmp;
							}
						}
						motionSensorId++;
					}
				}
			}
			else
				dataPresented = false;
		} catch (Exception e) {
		}
		try {
			fn.call();
		} catch (Exception e) {
		}
	}
	
	public void updateReles(int releId,int value,int releGroupIndex)
	{
		try {
			int bitsC = dataBlockClass[releGroupIndex].bits.length;
			int startMemByte = releStartIndex+((releId - (releId % bitsC))/bitsC)*2+1;
			int tt = dataBlockClass[releGroupIndex].StartIndex+((releId - (releId % bitsC))/bitsC)*2;
			int memByte = MainActivity.sts[tt];
			int bitPos = dataBlockClass[releGroupIndex].bits[releId % bitsC];
			byte val;
			if(value == 1)
				val = setBit((byte) memByte,bitPos);
			else
				val = resetBit((byte) memByte,bitPos);
			int[] outData = new int[5];
			outData[0] = MainActivity.user_id;
			outData[1] = MainActivity.server_commands.out;
			outData[2] = startMemByte+((MainActivity.isTcp)?0:memInIndex);
			outData[3] = 1;
			outData[4] = val;
			if(MainActivity.writeLogs)
				MainActivity.sent += "prepareCommand = true(updateRele)\n";
			MainActivity.preparingCommand = true;
			MainActivity.b = outData;
		} catch (Exception e) {
			if(MainActivity.writeLogs)
				MainActivity.sent+="\nupdateReles error : "+e.getMessage()+"\n";
		}
	}
	public void updateLights(int lightId,int value,int lightGroupIndex)
	{
		int startMemByte = lightStartIndex+lightId+1;
		int[] outData = new int[5];
		outData[0] = MainActivity.user_id;
		outData[1] = MainActivity.server_commands.out;
		outData[2] = startMemByte+((MainActivity.isTcp)?0:memInIndex);
		outData[3] = 1;
		outData[4] = value;
		MainActivity.b = outData;
	}
	
	public void setAlarm(String inps)
	{
		int[] input = MainActivity.sts;
		int[] outData = new int[5];
		Boolean inp = (inps == "true");
		outData[0] = MainActivity.user_id;
		outData[1] = MainActivity.server_commands.mis;
		outData[2] = alarmByte+((MainActivity.isTcp)?0:memMisIndex);
		outData[3] = 1;
		if(inp)
			outData[4] = setBit((byte) input[memMisIndex+alarmByte-1], alarmBit);
		else
			outData[4] = resetBit((byte) input[memMisIndex+alarmByte-1], alarmBit);
		MainActivity.b = outData;
	}
	
	public Boolean getAlarm()
	{
		int[] input = MainActivity.sts;		
		int alarmb = readBit((byte) input[memMisIndex+alarmByte-1], alarmBit);
		Boolean out = (alarmb == 1);
		alarmState = out;
		return out;
	}
	
	public void setFire(String inps)
	{
		Boolean inp = (inps == "true");
		int[] input = MainActivity.sts;
		int[] outData = new int[5];
		outData[0] = MainActivity.user_id;
		outData[1] = MainActivity.server_commands.mis;
		outData[2] = fireByte+((MainActivity.isTcp)?0:memMisIndex);
		outData[3] = 1;
		if(inp)
			outData[4] = setBit((byte) input[memMisIndex+fireByte-1], fireOnBit);
		else
			outData[4] = resetBit((byte) input[memMisIndex+fireByte-1], fireOnBit);
		MainActivity.b = outData;
	}
	
	public Boolean getFire()
	{
		int[] input = MainActivity.sts;		
		int alarmb = readBit((byte) input[memMisIndex+alarmByte-1], fireOnBit);
		Boolean out = (alarmb == 1);
		alarmState = out;
		return out;
	}
	
	public void setMuteFire(String inps)
	{
		Boolean inp = (inps == "true");
		int[] input = MainActivity.sts;
		int[] outData = new int[5];
		outData[0] = MainActivity.user_id;
		outData[1] = MainActivity.server_commands.mis;
		outData[2] = fireByte+((MainActivity.isTcp)?0:memMisIndex);
		outData[3] = 1;
		if(inp)
			outData[4] = setBit((byte) input[memMisIndex+fireByte-1], fireMuteBit);
		else
			outData[4] = resetBit((byte) input[memMisIndex+fireByte-1], fireMuteBit);
		MainActivity.b = outData;
	}
	
	public Boolean getMuteFire()
	{
		int[] input = MainActivity.sts;		
		int alarmb = readBit((byte) input[memMisIndex+alarmByte-1], fireMuteBit);
		Boolean out = (alarmb == 1);
		alarmState = out;
		return out;
	}
	public void setSenario(int number)
	{
		int[] outData = new int[5];
		outData[0] = MainActivity.user_id;
		outData[1] = MainActivity.server_commands.mis;
		outData[2] = senarioByteMis+((MainActivity.isTcp)?0:memMisIndex);
		outData[3] = 1;
		outData[4] = number;
		MainActivity.b = outData;
	}
}
