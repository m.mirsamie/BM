package com.gcom.bm;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class FireClass {
	Context mContext;
	public static int refreshRate = 1000;
	public static int blinkRate = 1500;
	
	public FireClass(Context inContext) {
		mContext = inContext;
	}
	public String fireSensorToJson(AlarmSensorClass[] inp)
	{
		String out = "";
		for(int i = 0;i < inp.length;i++)
		{
			AlarmSensorClass tmp = inp[i];
			out += ((out!="")?",":"")+"{\"id\":"+String.valueOf(tmp.id)+",\"isOn\":"+((tmp.isOn)?"true":"false")+"}";
		}
		return ("[" + out + "]");
	}
	
	@JavascriptInterface
	public int getRefreshRate()
	{
		return(refreshRate);
	}
	
	@JavascriptInterface
	public int getBlinkRate()
	{
		return(blinkRate);
	}
	
	@JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_LONG).show();
    }
	
	@JavascriptInterface
	public String getSensors()
	{
		String out = "";
		MainActivity.allData.refreshData();
    	if(MainActivity.allData.dataPresented &&  !MainActivity.allData.offline)
    		out = fireSensorToJson(MainActivity.allData.alarmSensors);
		return out;
	}
	
	@JavascriptInterface
	public String getAlarmState()
	{
		String out = "NaN";
		MainActivity.allData.refreshData();
    	if(MainActivity.allData.dataPresented &&  !MainActivity.allData.offline)
    	{
    		Boolean tmp = MainActivity.allData.getFire();
    		out = (tmp)?"true":"false";
    	}
		return out;
	}
	
	@JavascriptInterface
	public void toggleAlarm()
	{
		MainActivity.allData.refreshData();
		String lastst = getAlarmState();
    	if(MainActivity.allData.dataPresented &&  !MainActivity.allData.offline)
			MainActivity.allData.setFire((lastst=="true")?"false":"true");
	}
	@JavascriptInterface
	public String getAlarmMuteState()
	{
		String out = "NaN";
		MainActivity.allData.refreshData();
    	if(MainActivity.allData.dataPresented &&  !MainActivity.allData.offline)
    	{
    		Boolean tmp = MainActivity.allData.getMuteFire();
    		MainActivity.allData.fireState = tmp;
    		out = (tmp)?"true":"false";
    	}
		return out;
	}
	
	@JavascriptInterface
	public void toggleAlarmMute()
	{
		MainActivity.allData.refreshData();
		String lastst = getAlarmMuteState();
    	if(MainActivity.allData.dataPresented &&  !MainActivity.allData.offline)
			MainActivity.allData.setMuteFire((lastst=="true")?"false":"true");

	}
}
