package com.gcom.bm;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import android.content.Context;
import android.widget.Toast;


public class NetworkDataReader {
	public int port = 10000;
	public String ipAddr = "192.168.1.77";
	public Context mContext;
	public int bufferSize = 50;
	public boolean connected = false;
	private Socket clientSocket;
	private byte[] output;
	public NetworkDataReader(Context inContext , Socket inSocket)
	{
		mContext = inContext;
		clientSocket = inSocket;
		/*
		if(inPort >0)
			port = inPort;
		if(inIpAddr != "")
			ipAddr = inIpAddr;
		try {
			clientSocket = new Socket(ipAddr, port);
			connected = true;
		} catch (UnknownHostException e) {
			Toast.makeText(mContext, "error1 :"+e.getMessage(), Toast.LENGTH_LONG).show();
		} catch (IOException e) {
			Toast.makeText(mContext, "error2 :"+e.getMessage(), Toast.LENGTH_LONG).show();
		}catch (Exception e) {
			Toast.makeText(mContext, "error3 :"+e.getMessage(), Toast.LENGTH_LONG).show();
		}
		*/
	}
	/*
	public NetworkDataReader(Context inContext , int inPort)
	{
		mContext = inContext;
		if(inPort >0)
			port = inPort;
		try {
			clientSocket = new Socket(ipAddr, port);
			connected = true;
		} catch (UnknownHostException e) {
		} catch (IOException e) {
		}
	}
	public NetworkDataReader(Context inContext , String inIpAddr)
	{
		mContext = inContext;
		if(inIpAddr != "")
			ipAddr = inIpAddr;
		try {
			clientSocket = new Socket(ipAddr, port);
			connected = true;
		} catch (UnknownHostException e) {
		} catch (IOException e) {
		}
	}	
	public NetworkDataReader(Context inContext)
	{
		mContext = inContext;
		try {
			clientSocket = new Socket(ipAddr, port);
			connected = true;
		} catch (UnknownHostException e) {
		} catch (IOException e) {
		}
	}
	*/
	public byte[] sendData(byte[] data)
	{
		return(ReallySendData(data,bufferSize));
	}
	public byte[] sendData(byte[] data,int bfs)
	{
		return(ReallySendData(data,bfs));
	}
	public byte[] ReallySendData(byte[] dataB,int bfs)
	{
		if(connected)
		{
			String data = new String(dataB);
			byte[] bf = new byte[bfs];
			int i = 0;
			try {
				Toast.makeText(mContext, "sending '"+data+"'", Toast.LENGTH_LONG).show();
				DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
				BufferedInputStream inFromServer = new BufferedInputStream(clientSocket.getInputStream());
				outToServer.writeBytes(data + '\n');
				for(i = 0;i < bfs;i++)
				{
					int b = inFromServer.read();
					if(b != -1)
						bf[i] = (byte)b;
				}
				String dataO = new String(bf);
				Toast.makeText(mContext, "recievd : '"+dataO+"'", Toast.LENGTH_LONG).show();
				output = bf;
			} catch (IOException e) {
				
			}
		}
		else
			Toast.makeText(mContext, "Not Connected", Toast.LENGTH_LONG).show();
		return(output);
	}
	public void close()
	{
		try {
			if(connected)
				clientSocket.close();
		} catch (IOException e) {
			
		}
	}
}